#!/bin/bash

# In some cases you need to allow /some/where/sst.sh without password
# in sudo
trap 'dont touch' 15
while [ true ] ; do
        sudo /some/where/sst.sh
done

