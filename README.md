Shell-Support-Tool
==================

Because of inquiries from 1st level to get access to servers without the possibility to crash everything i created a small bash/dialog framework to give users without shell knowledge the possibility to read out data or initiate scripted tasks. 

Please help and add good standard functions for a general system overview. Company specific functions can be added by company users themselves in their forks. =)

![Screenshot 640](screenshot.png)


Copyright 2015 Martin Treide-Heuser
This software/scripts is/are licensed under GPL

https://gitlab.com/martin-treideheuser/Shell-Support-Tool
