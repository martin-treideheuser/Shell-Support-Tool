#!/bin/bash

##############################################
#
# 03/2015 martin.treideheuser@gmail.com
#
##############################################

#Variables
_temp="/tmp/answer.$$"
PN=`basename "$0"`
VER='0.01'
#dialog 2>$_temp
#DVER=`cat $_temp | head -1`
CONFIGFILE=~/.sst
SSHPARAMETER="-F /dev/null"

finish() { #{{{
	echo " "
	echo "You made my day. :)"

} #}}}

setup() { #{{{
	dialog --backtitle "Setup"\
                --inputbox "Enter your ssh username" 8 52 2>$_temp
	USERNAME=`cat $_temp`
	if [ -z "$USERNAME" ]; then
		dialog --infobox "Setup not finished... exiting..." 3 40 ; sleep 3
		clear
		exit
	fi
	echo $USERNAME > $CONFIGFILE
} #}}}

SystemState() { #{{{


	### Main dialog needs to be at the bottom of this function	
	dialog --backtitle "SystemState`echo -e "\t\t"`-   Configured SSH Username: $USERNAME   -   `date`" --title " V. $VER "\
        --cancel-label "Quit" \
        --menu "Move using [UP] [DOWN], [Enter] to select" 17 60 10\
	"ProcessInfo" "CPU: `cat /proc/cpuinfo |grep -c processor` - Load: `cat /proc/loadavg |awk {'print $2'}` (5min)"\
	"HarddriveInfo" "Harddrive information"\
	"-----------" "----------------------------"\
        Return "Return" 2>$_temp

	opt=${?}
    	if [ $opt != 0 ]; then rm $_temp; exit; fi
    	menuitem=`cat $_temp`
    	echo "menu=$menuitem"
    	case $menuitem in
		ProcessInfo) (top -d 5);;
		Return) return;;
    	esac


	return 
} #}}}

CheckEnviroment() { #{{{
	DIALOGEXIST=`which dialog | wc -l`
	if [ "$DIALOGEXIST" -eq "0" ]; then
		echo "Dialog package not found. Please install it. 'apt-get install dialog'"
		exit
	fi

	USERNAME=`cat $CONFIGFILE`

	if [ -z $USERNAME ]; then
		setup
	fi
} #}}}

# This has to be located at the end of the script. New functions have to be above.

main_menu() { #{{{

	trap finish EXIT
	CheckEnviroment

	dialog --backtitle "Shell Support Tool`echo -e "\t\t"`-   Configured SSH Username: $USERNAME   -   `date`" --title " V. $VER "\
        --cancel-label "Quit" \
        --menu "Move using [UP] [DOWN], [Enter] to select" 17 60 10\
	SystemState "Check local system state"\
	"-----------" "----------------------------"\
        Setup "Run Setup" 2>$_temp

	opt=${?}
    	if [ $opt != 0 ]; then rm $_temp; exit; fi
    	menuitem=`cat $_temp`
    	echo "menu=$menuitem"
    	case $menuitem in
		SystemState) SystemState;;
		Setup) setup;;
    	esac


} #}}}

# Bikini_Bottom_Main_Loop
trap "main_menu" 0 1 2 3 15
while true; do #{{{
	main_menu
done #}}}


